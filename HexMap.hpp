﻿#ifndef HEXMAP_H
#define HEXMAP_H

#include <deque>
#include <set>
#include <list>
#include <functional>
#include <SFML/Graphics.hpp>
#include "Array2d.hpp"
#include "Compare.hpp"
#include "rng.hpp"

typedef std::set<sf::Vector2i, Vector2iCompare> VectorSet;


struct cubepoint {
	float x;
	float y;
	float z;
	cubepoint() { x = 0; y = 0; z = 0; }
	cubepoint(float cx, float cy, float cz) { x = cx; y = cy; z = cz; }
	cubepoint operator+= (cubepoint& cp) { x += cp.x; y += cp.y; z += cp.z; return *this; }
	cubepoint operator+ (cubepoint& cp) { return cubepoint(*this) += cp; }
	cubepoint operator-= (cubepoint& cp) { x -= cp.x; y -= cp.y; z -= cp.z; return *this; }
	cubepoint operator- (cubepoint& cp) { return cubepoint(*this) -= cp; }
};


namespace hexgrid {
	extern const sf::Vector2i directions[6];
	// Get the coordinate in the given direction from the given hex
	sf::Vector2i neighbor(const sf::Vector2i& centerAxial, char dir);
	// Get all coordinates neighboring the given hex
	VectorSet& neighbors(const sf::Vector2i& centerAxial, VectorSet& n);
	// Get all hex coordinates within the given radius from the center hex
	VectorSet& area(sf::Vector2i centerAxial, int radius, VectorSet& n);
	// Get all hex coordinates with the given distance (radius) from the center hex
	VectorSet& ring(sf::Vector2i centerAxial, int radius, VectorSet& n);
	// Convert offset coordinates to axial hex coordinates
	sf::Vector2f offsetToAxial(sf::Vector2f v, bool verticalHexes);
	// Convert axial hex coordinates to offset coordinates
	sf::Vector2f axialToOffset(sf::Vector2f v, bool verticalHexes);
	// Convert offset coordinates to axial hex coordinates
	sf::Vector2i offsetToAxial(sf::Vector2i v, bool verticalHexes);
	// Convert axial hex coordinates to offset coordinates
	sf::Vector2i axialToOffset(sf::Vector2i v, bool verticalHexes);
	// Round a floating point axial coordinate to the nearest hex
	sf::Vector2f roundHex(sf::Vector2f hex);
	// Get the distance in axial coordinates between two hexes
	float distAxial(const sf::Vector2f& a, const sf::Vector2f& b);
	// Get the distance in axial coordinates between two hexes
	float distAxial(const sf::Vector2i& a, const sf::Vector2i& b);
}


template<typename T>
class HexMap {
private:
	class FloodFill {
		sf::Vector2i start_;
		int totalSize_;
		// set to fill
		VectorSet* container_;
		int sizeLimit_;
		// stores neighboring tiles
		VectorSet adj_;
		// new tiles to query
		std::vector<sf::Vector2i> frontier_;
		// temporary frontier to populate and swap with the old
		std::vector<sf::Vector2i> newFrontier_;
		// which tiles have we already checked?
		VectorSet* seen_;
		// parent map
		HexMap* hm_;
		std::function<bool(T&)>* condition_;
	public:

		FloodFill(int sizeLimit, VectorSet* seen, HexMap<T>* hm, std::function<bool(T&)>& condition) :
			totalSize_(0), container_(nullptr), sizeLimit_(sizeLimit),
			seen_(seen), hm_(hm), condition_(&condition) {
		}

		bool iterate() {
			for (auto& f : frontier_) {
				hm_->clipToBounds(neighbors(f, adj_));
				for (auto it = adj_.begin(); it != adj_.end(); it++) {
					if (seen_->find(*it) != seen_->end()) {
						continue;
					}
					seen_->insert(*it);
					sf::Vector2i off = axialToOffset(*it);
					if (!(*condition_)(hm_->hexes_(off.x, off.y))) {
						continue;
					}
					if (container_ != nullptr) {
						container_->insert(*it);
					}
					newFrontier_.push_back(*it);
					totalSize_++;
					if (totalSize_ >= sizeLimit_) {
						return false;
					}
				}
			}
			if (newFrontier_.empty()) {
				return false;
			}
			frontier_.clear();
			std::swap(frontier_, newFrontier_);
			return true;
		}

		void run() {
			while (iterate()) {}
		}

		int getSize() {
			return totalSize_;
		}

		void initFill(sf::Vector2i start) {
			frontier_.clear();
			newFrontier_.clear();
			if (!(*condition_)(hm_->getAxial(start.x, start.y))) {
				return;
			}
			start_ = start;
			totalSize_ = 1;
			container_->insert(start);
			frontier_.push_back(start);
			seen_->insert(start);
		}

		void setOutputContainer(VectorSet* container) {
			container_ = container;
		}
	};

	int hexRad_;
	sf::Vector2f hexSize_;
	sf::Vector2f hexAdvance_;
	sf::Vector2f mapOrigin_;
	sf::Vector2i mapSize_;
	sf::Vector2f hexExtent_;
	sf::Vector2i cursorPos_;
	bool vertical_;
	float angle_;
	// Tile data
	Array2D<T> hexes_;
public:

	sf::Vector2f roundvf(sf::Vector2f p) {
		p.x = roundf(p.x);
		p.y = roundf(p.y);
		return p;
	}

	HexMap() {
		hexRad_ = 18;
		hexSize_ = { 36, 32 };
		hexAdvance_ = { 27, 32 };
		mapOrigin_ = { 18, 16 };
		vertical_ = false;
		angle_ = 0.0f;
	}


	int heuristic(sf::Vector2i& a, sf::Vector2i& b) {
		return (int)((abs(a.x - b.x) + abs(a.x + a.y - b.x - b.y) + abs(a.y - b.y)) / 2);
		//return abs(a.x - b.x) + abs(a.y - b.y);
	}

	const int BIG_COST = 1000000000;

	int moveCost(sf::Vector2i& current, sf::Vector2i& next) {
		return 1;
	}

	std::deque<sf::Vector2i>& getPath(std::deque<sf::Vector2i>& path, sf::Vector2i startAxial, sf::Vector2i goalAxial) {
		if (!isAxialInBounds(startAxial) || !isAxialInBounds(goalAxial)) {
			return path;
		}
		// traceback
		std::unordered_map<sf::Vector2i, sf::Vector2i, Vector2iHash> cameFrom;
		// combined cost of tiles leading to this one
		std::unordered_map<sf::Vector2i, int, Vector2iHash> costSoFar;
		// new tiles to query
		std::multimap<int, sf::Vector2i> frontier;
		sf::Vector2i start = startAxial;
		sf::Vector2i goal = goalAxial;
		VectorSet n;
		frontier.insert(std::pair<int, sf::Vector2i>(0, start));
		cameFrom[start] = start;
		costSoFar[start] = 0;
		while (!frontier.empty()) {
			auto current = frontier.begin()->second;
			frontier.erase(frontier.begin());
			if (current == goal) {
				break;
			}
			n.clear();
			for (auto next : neighbors(current, n)) {
				if (!isAxialInBounds(next) || !getAxial(next.x, next.y).hts->FLAGS[TS::WALKABLE]) {
					continue;
				}
				int newCost = costSoFar[current] + (next == goal ? 0 : moveCost(current, next));
				if (!costSoFar.count(next) || newCost < costSoFar[next]) {
					costSoFar[next] = newCost;
					int priority = newCost + heuristic(next, goal);
					frontier.insert(std::pair<int, sf::Vector2i>(priority, next));
					cameFrom[next] = current;
				}
			}
		}
		if (cameFrom.find(goal) == cameFrom.end()) {
			// A path was not found
			return path;
		}
		for (sf::Vector2i c = goal; c != start; c = cameFrom.find(c)->second) {
			path.push_front(c);
		}
		return path;
	}

	int getPathCost(sf::Vector2i startAxial, sf::Vector2i goalAxial) {
		if (!isAxialInBounds(startAxial) || !isAxialInBounds(goalAxial)) {
			return 9001;
		}
		// combined cost of tiles leading to this one
		std::unordered_map<sf::Vector2i, int, Vector2iHash> costSoFar;
		// new tiles to query
		std::multimap<int, sf::Vector2i> frontier;
		sf::Vector2i start = startAxial;
		sf::Vector2i goal = goalAxial;
		VectorSet n;
		frontier.insert(std::pair<int, sf::Vector2i>(0, start));
		costSoFar[start] = 0;
		while (!frontier.empty()) {
			auto current = frontier.begin()->second;
			frontier.erase(frontier.begin());
			if (current == goal) {
				break;
			}
			n.clear();
			for (auto next : neighbors(current, n)) {
				if (!isAxialInBounds(next) || !getAxial(next.x, next.y).hts->FLAGS[TS::WALKABLE]) {
					continue;
				}
				int newCost = costSoFar[current] + moveCost(current, next);
				if (!costSoFar.count(next) || newCost < costSoFar[next]) {
					costSoFar[next] = newCost;
					int priority = newCost + heuristic(next, goal);
					frontier.insert(std::pair<int, sf::Vector2i>(priority, next));
				}
			}
		}
		auto distance = costSoFar.find(goal);
		if (distance == costSoFar.end()) {
			// A path was not found
			return 9001;
		}
		return distance->second;
	}

	const int& getHexRadius(int zoom) const {
		return hexRad_;
	}

	const sf::Vector2f& getHexSize(int zoom) const {
		return hexSize_;
	}

	const sf::Vector2f& getHexAdvance(int zoom) const {
		return hexAdvance_;
	}

	const sf::Vector2f& getMapOrigin(int zoom) const {
		return mapOrigin_;
	}

	const sf::Vector2i& getMapSize() const {
		return mapSize_;
	}

	bool usingVerticalHexes() const {
		return vertical_;
	}

	float getHexAngle() const {
		return angle_;
	}

	void setHexMetrics(int hexRadius, bool useVerticalHexes, float hexAngle = 1.0f, bool roundToNearestPixel = true) {
		static const float hexShortSide = 0.8660254037844f;
		hexRad_ = hexRadius;
		vertical_ = useVerticalHexes;
		angle_ = hexAngle;
		int rad2 = hexRadius * 2;
		if (useVerticalHexes) {
			hexSize_ = sf::Vector2f(rad2 * hexShortSide, rad2 * angle_);
			hexAdvance_ = sf::Vector2f(hexSize_.x, hexSize_.x * hexShortSide * angle_);
			mapOrigin_ = (sf::Vector2f)hexSize_ / 2.0f;
		}
		else {
			hexSize_ = sf::Vector2f((float)rad2, (float)(rad2 * hexShortSide * angle_));
			hexAdvance_ = sf::Vector2f(hexSize_.y * hexShortSide / angle_, hexSize_.y);
			mapOrigin_ = (sf::Vector2f)hexSize_ / 2.0f;
		}
		if (roundToNearestPixel) {
			hexSize_ = roundvf(hexSize_);
			hexAdvance_ = roundvf(hexAdvance_);
		}
	}

	void setHexMetrics(sf::Vector2f hexSize, sf::Vector2f hexAdvance, bool useVerticalHexes) {
		angle_ = 0.0f;
		hexSize_ = hexSize;
		vertical_ = useVerticalHexes;
		if (useVerticalHexes) {
			hexRad_ = hexSize_.y / 2.0f;
			hexAdvance_ = hexAdvance;
			mapOrigin_ = (sf::Vector2f)hexSize_ / 2.0f;
		}
		else {
			hexRad_ = hexSize_.x / 2.0f;
			hexAdvance_ = hexAdvance;
			mapOrigin_ = (sf::Vector2f)hexSize_ / 2.0f;
		}
	}

	virtual void init(int width, int height) {
		int currentVertices = 0;
		sf::Vector2i chunkOffset = { 0, 0 };
		sf::Vector2i tileOffset = { 0, 0 };
		// Generate all hex tiles and their corresponding vertices
		mapSize_ = { width, height };
		hexes_.set(width, height);
		hexExtent_ = { (mapSize_.x - 1) * hexAdvance_.x, (mapSize_.y - 1) * hexAdvance_.y };
	}

	sf::Vector2f hexToPixel(sf::Vector2f hex) const {
		if (vertical_) {
			hex.x = hexAdvance_.x * (hex.x + hex.y / 2.0f);
			hex.y = hexAdvance_.y * hex.y;
		}
		else {
			hex.y = hexAdvance_.y * (hex.y + hex.x / 2.0f);
			hex.x = hexAdvance_.x * hex.x;
		}
		return hex;
	}

	sf::Vector2f pixelToHex(sf::Vector2f pixel) const {
		//hexAdvance_ = { roundf(hexRad_ * ROOT3), roundf(hexRad_ * 3.0 / 2.0) }
		// compute it
		cubepoint c;
		if (vertical_) {
			pixel.x = pixel.x / hexAdvance_.x - pixel.y / hexAdvance_.y * 0.5f;
			pixel.y = pixel.y / hexAdvance_.y;
		}
		else {
			pixel.y = pixel.y / hexAdvance_.y - pixel.x / hexAdvance_.x * 0.5f;
			pixel.x = pixel.x / hexAdvance_.x;
		}
		// round it
		return hexgrid::roundHex(pixel);
	}

	bool isAxialInBounds(sf::Vector2i posAxial) const {
		posAxial.x -= -floorf(posAxial.y / 2.0);
		if (posAxial.x < 0 || posAxial.x >= mapSize_.x || posAxial.y < 0 || posAxial.y >= mapSize_.y) {
			return false;
		}
		return true;
	}

	bool isOffsetInBounds(sf::Vector2i posOffset) const {
		if (posOffset.x < 0 || posOffset.x >= mapSize_.x || posOffset.y < 0 || posOffset.y >= mapSize_.y) {
			return false;
		}
		return true;
	}

	VectorSet& clipToBounds(VectorSet& boundedAxial) {
		for (auto it = boundedAxial.begin(); it != boundedAxial.end();) {
			if (!isAxialInBounds(*it)) {
				it = boundedAxial.erase(it);
				continue;
			}
			it++;
		}
		return boundedAxial;
	}

	VectorSet& clip(VectorSet& listAxial, std::function<bool(T&)> condition) {
		for (auto it = listAxial.begin(); it != listAxial.end();) {
			if (!condition(getAxial(it->x, it->y))) {
				it = listAxial.erase(it);
				continue;
			}
			it++;
		}
		return listAxial;
	}

	T& getAxial(int x, int y) {
		double tempX = x, tempY = y;
		tempX += floor((tempY - ((int)tempY & 1)) / 2.0);
		return hexes_((int)tempX, (int)tempY);
	}

	T& getOffset(int x, int y) {
		return hexes_(x, y);
	}

	VectorSet& floodSelect(VectorSet& fill, sf::Vector2i start, int sizeLimit, std::function<bool(T&)>& condition) {
		VectorSet seen;
		FloodFill f(sizeLimit, &seen, this, condition);
		f.setOutputContainer(&fill);
		f.initFill(start);
		f.run();
		return fill;
	}

	std::vector<VectorSet>& floodSelectParallel(std::vector<VectorSet>& fill, std::vector<sf::Vector2i>& start, int sizeLimit, std::function<bool(T&)>& condition) {
		VectorSet seen;
		// pointers to the VectorSets, so we can remove them
		// as they finish filling
		std::vector<FloodFill> fillPtr;
		int index = 0;
		for (auto& f : fill) {
			fillPtr.emplace_back(sizeLimit, &seen, this, condition);
			fillPtr.back().setOutputContainer(&f);
			fillPtr.back().initFill(start[index]);
			index++;
		}
		while (!fillPtr.empty()) {
			for (auto it = fillPtr.begin(); it != fillPtr.end();) {
				if (!it->iterate()) { // the FloodFill has either reached capacity or run out of tiles
					it = fillPtr.erase(it);
					continue;
				}
				it++;
			}
		}
		return fill;
	}

	int floodSelectSize(sf::Vector2i start, std::function<bool(T&)>& condition) {
		VectorSet seen;
		FloodFill f(std::numeric_limits<int>::max(), &seen, this, condition);
		f.initFill(start);
		f.run();
		return f.getSize();
	}
};

#endif
