﻿#include <fstream>
#include <sstream>
#include <iostream>
#include "HexMap.hpp"
#include "clamp.hpp"
#include <unordered_map>
#include <map>

///////////////
// HexMap    //
///////////////

namespace hexgrid {

	const sf::Vector2i directions[6] = { { 1, -1 },{ 0, -1 },{ -1, 0 },{ -1, 1 },{ 0, 1 },{ 1, 0 } };

	sf::Vector2i neighbor(const sf::Vector2i& centerAxial, char dir) {
		return sf::Vector2i(centerAxial.x + directions[dir].x, centerAxial.y + directions[dir].y);
	}

	VectorSet& neighbors(const sf::Vector2i& centerAxial, VectorSet& n) {
		for (int x = 0; x < 6; x++) {
			n.insert(sf::Vector2i(centerAxial.x + directions[x].x, centerAxial.y + directions[x].y));
		}
		return n;
	}

	VectorSet& area(sf::Vector2i centerAxial, int radius, VectorSet& n) {
		//var results = []
		//for each - N ≤ dx ≤ N :
		//	for each max(-N, -dx - N) ≤ dy ≤ min(N, -dx + N) :
		//		var dz = -dx - dy
		//		results.append(cube_add(center, Cube(dx, dy, dz)))
		cubepoint cp((float)centerAxial.x, (float)radius, (float)centerAxial.y);
		cubepoint d;
		cubepoint np;
		for (d.x = -cp.y; d.x <= cp.y; d.x++) {
			for (d.y = std::max(-cp.y, -d.x - cp.y); d.y <= std::min(cp.y, -d.x + cp.y); d.y++) {
				d.z = -d.x - d.y;
				np = d + cp;
				n.insert(sf::Vector2i((int)np.x, (int)np.z));
			}
		}
		return n;
	}

	VectorSet& ring(sf::Vector2i centerAxial, int radius, VectorSet& n) {
		if (radius == 0) {
			n.insert(centerAxial);
			return n;
		}
		centerAxial.y += radius;
		for (int d = 0; d < 6; d++) {
			for (int r = 0; r < radius; r++) {
				n.insert(centerAxial);
				centerAxial = neighbor(centerAxial, d);
			}
		}
		return n;
	}

	sf::Vector2f offsetToAxial(sf::Vector2f v, bool verticalHexes) {
		//x = col - (row - (row & 1)) / 2
		//z = row
		//y = -x - z
		if (verticalHexes) {
			v.x -= floor((v.y - ((int)v.y & 1)) / 2.0f);
		}
		else {
			v.y -= floor((v.x - ((int)v.x & 1)) / 2.0f);
		}
		return v;
	}

	sf::Vector2f axialToOffset(sf::Vector2f v, bool verticalHexes) {
		if (verticalHexes) {
			v.x += floor((v.y - ((int)v.y & 1)) / 2.0f);
		}
		else {
			v.y += floor((v.x - ((int)v.x & 1)) / 2.0f);
		}
		return v;
	}

	sf::Vector2i offsetToAxial(sf::Vector2i v, bool verticalHexes) {
		if (verticalHexes) {
			v.x -= (v.y - (v.y & 1)) / 2;
		}
		else {
			v.y -= (v.x - (v.x & 1)) / 2;
		}
		return v;
	}

	sf::Vector2i axialToOffset(sf::Vector2i v, bool verticalHexes) {
		if (verticalHexes) {
			v.x += (v.y - (v.y & 1)) / 2;
		}
		else {
			v.y += (v.x - (v.x & 1)) / 2;
		}
		return v;
	}

	sf::Vector2f roundHex(sf::Vector2f hex) {
		cubepoint c;
		c.x = hex.x;
		c.z = hex.y;
		c.y = -c.x - c.z;
		cubepoint r(roundf(c.x), roundf(c.y), roundf(c.z));
		cubepoint diff(abs(r.x - c.x), abs(r.y - c.y), abs(r.z - c.z));
		if (diff.x > diff.y && diff.x > diff.z) { r.x = -r.y - r.z; }
		else if (diff.y > diff.z) { r.y = -r.x - r.z; }
		else { r.z = -r.x - r.y; }
		hex.x = r.x;
		hex.y = r.z;
		return hex;
	}

	float distAxial(const sf::Vector2f& a, const sf::Vector2f& b) {
		return (abs(a.x - b.x) + abs(a.x + a.y - b.x - b.y) + abs(a.y - b.y)) / 2;
	}

	float distAxial(const sf::Vector2i& a, const sf::Vector2i& b) {
		return (float)((abs(a.x - b.x) + abs(a.x + a.y - b.x - b.y) + abs(a.y - b.y)) / 2);
	}
}